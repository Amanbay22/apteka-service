FROM gradle:8.5.0-jdk17 as builder
WORKDIR /app
COPY . /app/.

RUN ./gradlew clean
RUN ./gradlew build

FROM openjdk:17-jdk-slim
WORKDIR /app
COPY --from=builder /app/build/libs/apteka-diplom-0.0.1-SNAPSHOT.jar .
EXPOSE 8080

CMD ["java", "-jar", "apteka-diplom-0.0.1-SNAPSHOT.jar"]
