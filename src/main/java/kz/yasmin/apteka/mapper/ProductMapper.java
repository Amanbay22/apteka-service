package kz.yasmin.apteka.mapper;

import kz.yasmin.apteka.domain.entity.ProductEntity;
import kz.yasmin.apteka.dto.product.ProductCreateDto;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

public class ProductMapper implements Converter<ProductCreateDto, ProductEntity> {

    @Override
    public ProductEntity convert(MappingContext<ProductCreateDto, ProductEntity> context) {
        var dto = context.getSource();
        var entity = new ProductEntity();

        entity.setDescription(dto.getDescription());
        entity.setForm(dto.getForm());
        entity.setDosage(dto.getDosage());
        entity.setManufacturer(dto.getManufacturer());
        entity.setDosageChild(dto.getDosageChild());
        entity.setDosageAdult(dto.getDosageAdult());
        entity.setDosagePeriod(dto.getDosagePeriod());
        entity.setPhotoPaths(dto.getPhotoPaths());
        entity.setShortInfo(dto.getShortInfo());
        entity.setUsageInfo(dto.getUsageInfo());
        entity.setName(dto.getName());
        entity.setType(dto.getType());

        return entity;
    }
}
