package kz.yasmin.apteka.mapper;

import kz.yasmin.apteka.domain.entity.AddressEntity;
import kz.yasmin.apteka.dto.client.AddressCreateDto;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

public class AddressMapper implements Converter<AddressCreateDto, AddressEntity> {

    @Override
    public AddressEntity convert(MappingContext<AddressCreateDto, AddressEntity> context) {
        var dto = context.getSource();

        var entity = new AddressEntity();
        entity.setDistrict(dto.getDistrict());
        entity.setStreet(dto.getStreet());
        entity.setApartmentNumber(dto.getApartmentNumber());
        entity.setHouseNumber(dto.getHouseNumber());
        entity.setComment(dto.getComment());

        return entity;
    }
}
