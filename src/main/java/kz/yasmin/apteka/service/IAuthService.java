package kz.yasmin.apteka.service;

import kz.yasmin.apteka.domain.entity.Roles;
import kz.yasmin.apteka.dto.auth.AuthRequestDto;
import kz.yasmin.apteka.dto.auth.AuthResponseDto;
import kz.yasmin.apteka.dto.auth.RegRequestDto;
import kz.yasmin.apteka.dto.auth.ResetPasswordDto;

import java.util.Set;

public interface IAuthService {
  AuthResponseDto register(RegRequestDto request, Set<Roles> roles);

  AuthResponseDto authenticate(AuthRequestDto request);

  void resetPassword(ResetPasswordDto dto);

  AuthResponseDto registerAdmin(RegRequestDto regDto, String basic);
}
