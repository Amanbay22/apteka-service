package kz.yasmin.apteka.service;

import kz.yasmin.apteka.dto.pharmacy.PharmacyCreateDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyGetDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyRs;
import kz.yasmin.apteka.dto.pharmacy.PharmacySimpleDto;

import java.util.List;

public interface IPharmacyService {
    PharmacyRs createPharmacy(PharmacyCreateDto dto);
    List<PharmacySimpleDto> getPharmacies();
}
