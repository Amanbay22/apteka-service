package kz.yasmin.apteka.service;

import kz.yasmin.apteka.domain.entity.ProductTypeEnum;

import java.util.List;

public interface IUserService {
    void setUserIssuesByUsername(List<ProductTypeEnum> issues);
}
