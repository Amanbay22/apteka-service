package kz.yasmin.apteka.service;

import kz.yasmin.apteka.dto.file.FileGetDto;
import kz.yasmin.apteka.dto.file.FilesGetDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IUploadService {
    FileGetDto saveFile(MultipartFile file);
    FilesGetDto saveFiles(List<MultipartFile> files);
}
