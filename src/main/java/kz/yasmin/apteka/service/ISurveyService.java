package kz.yasmin.apteka.service;

import kz.yasmin.apteka.dto.survey.SurveyDto;

public interface ISurveyService {
    void saveSurveyData(SurveyDto dto);
}
