package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.domain.entity.AddressEntity;
import kz.yasmin.apteka.domain.repo.AddressRepo;
import kz.yasmin.apteka.domain.repo.UserRepository;
import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.client.*;
import kz.yasmin.apteka.service.IClientService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements IClientService {

    private final UserRepository userRepo;
    private final AddressRepo addressRepo;
    private final ModelMapper mapper;

    @Override
    @Transactional
    public AddressRs createClientAddress(UserDetails user, AddressCreateDto dto) {
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();
        var address = mapper.map(dto, AddressEntity.class);
        var addressEntity = addressRepo.save(address);
        client.getAddresses().add(addressEntity);
        userRepo.save(client);

        return new AddressRs(200, "Address add to client", addressEntity.getId());
    }

    @Override
    @Transactional
    public BasicRs changeClientInfo(UserDetails user, ClientInfoDto dto) {
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();

        if (StringUtils.hasText(dto.getPhoneNumber())) {
            client.setUsername(dto.getPhoneNumber());
        }
        if (StringUtils.hasText(dto.getFirstname())) {
            client.setFirstname(dto.getFirstname());
        }
        if (StringUtils.hasText(dto.getLastname())) {
            client.setLastname(dto.getLastname());
        }
        if (StringUtils.hasText(dto.getSex())) {
            client.setSex(dto.getSex());
        }

        return new BasicRs(200, "Client successfully updated");
    }

    @Override
    public ClientGetDto getClientInfo(UserDetails user) {
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();

        ClientGetDto dto = new ClientGetDto();
        dto.setFirstname(client.getFirstname());
        dto.setLastname(client.getLastname());
        dto.setSex(client.getSex());
        dto.setPhoneNumber(client.getUsername());
        List<AddressDto> addresses = client.getAddresses().stream()
                .map(a -> new AddressDto(a.getId(), a.getStreet() + " " + a.getHouseNumber()))
                .toList();
        dto.setAddresses(addresses);

        return dto;
    }
}
