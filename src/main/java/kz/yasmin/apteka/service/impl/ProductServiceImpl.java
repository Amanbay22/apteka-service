package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.domain.entity.*;
import kz.yasmin.apteka.domain.repo.*;
import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.order.OrderGetAdminDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyGetDto;
import kz.yasmin.apteka.dto.product.*;
import kz.yasmin.apteka.dto.review.ReviewGetDto;
import kz.yasmin.apteka.service.IProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements IProductService {

    private final ProductRepo productRepo;
    private final PharmacyRepo pharmacyRepo;
    private final ProductPriceRepo productPriceRepo;
    private final UserRepository userRepo;
    private final ReviewRepo reviewRepo;
    private final OrderSingleRepo orderSingleRepo;

    private final ModelMapper mapper;

    @Override
    @Transactional
    public BasicRs createProduct(UserDetails user, ProductCreateDto dto) {
        if (CollectionUtils.isEmpty(dto.getPhotoPaths())) {
            return new BasicRs(400, "Add pls photo path for images");
        }

        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();
        var pharmacy = pharmacyRepo.findById(client.getPharmacyId()).orElseThrow();

        Optional<ProductEntity> findProduct = productRepo.findByNameAndDosageAndType(dto.getName(), dto.getDosage(), dto.getType());
        var productPrice = new ProductPriceEntity();
        productPrice.setPharmacy(pharmacy);
        productPrice.setPrice(dto.getPrice());
        ProductPriceEntity save = productPriceRepo.save(productPrice);

        if (findProduct.isPresent()) {
            ProductEntity product = findProduct.get();
            product.getPrices().add(save);
            save.setProduct(product);

            productPriceRepo.save(save);
            return new BasicRs(200, "Product found, adding new price for him");
        } else {
            var entity = mapper.map(dto, ProductEntity.class);
            entity.setPrices(new ArrayList<>());
            entity.getPrices().add(save);
            productRepo.save(entity);
            save.setProduct(entity);

            productPriceRepo.save(productPrice);
            return new BasicRs(200, "Product created");
        }

    }

    @Override
    public List<ProductGetDto> getProductByType(ProductTypeEnum type) {
        return productRepo.findAllByType(type).stream()
                .map(this::mapProduct)
                .toList();
    }

    @Override
    public ProductFullDto getProductById(Long productId) {
        var product = productRepo.findById(productId).orElseThrow();
        List<ReviewEntity> reviews = reviewRepo.findByProduct(product);

        ProductFullDto full = new ProductFullDto();
        full.setTitle(product.getName() + " " + product.getDosage() + " " + product.getForm().getString());
        full.setPrice(getPriceFromList(product.getPrices()));
        full.setDosage(product.getDosage() + product.getForm().getString());
        full.setManufacturer(product.getManufacturer());
        full.setDosageFrom(product.getDosageChild() + product.getForm().getString());
        full.setDosageTo(product.getDosageAdult() + product.getForm().getString());
        full.setDosagePeriod(product.getDosagePeriod());
        full.setDescription(product.getDescription());
        full.setPhotoUrls(product.getPhotoPaths());
        full.setReviews(getReviews(reviews));

        full.setPharmacies(getPharmacies(product.getPrices()));

        return full;
    }

    @Override
    public List<ProductGetDto> getProductByClientIssues(UserDetails user) {
        if (user == null) {
            List<ProductEntity> all = productRepo.findAll();
            int size = Math.min(all.size(), 10);
            return productRepo.findAll().subList(0, size).stream()
                    .map(this::mapProduct)
                    .toList();
        }
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();

        List<ProductEntity> all = productRepo.findAllByTypeIn(client.getIssueTypes());
        int size = Math.min(all.size(), 10);
        return all.subList(0, size).stream()
                .map(this::mapProduct)
                .toList();


    }

    @Override
    public List<ProductSearchDto> getProductSearch(String search) {
        return productRepo.findAllByNameContainingIgnoreCase(search).stream()
                .map(p -> {
                    ProductSearchDto dto = new ProductSearchDto();
                    dto.setProductId(p.getId());
                    dto.setName(p.getName() + " " + p.getDosage() + " " + p.getForm());

                    return dto;
                })
                .toList();
    }

    @Override
    public List<ProductByPharmacyDto> getProductByPharmacy(UserDetails user) {
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();
        var productPrice = productPriceRepo.findAllByPharmacy_Id(client.getPharmacyId());
        return productPrice.stream()
                .map(pp -> {
                    var product = pp.getProduct();
                    var dto = new ProductByPharmacyDto();
                    dto.setName(product.getName());
                    dto.setForm(product.getForm());
                    dto.setType(product.getType());
                    dto.setDosage(product.getDosage());
                    dto.setDosagePeriod(product.getDosagePeriod());
                    dto.setDosageChild(product.getDosageChild());
                    dto.setDosageAdult(product.getDosageAdult());
                    dto.setManufacturer(product.getManufacturer());
                    dto.setDescription(product.getDescription());
                    dto.setUsageInfo(product.getUsageInfo());
                    dto.setShortInfo(product.getShortInfo());
                    dto.setPrice(pp.getPrice() + "₸");

                    return dto;
                })
                .toList();
    }

    @Override
    public List<OrderGetAdminDto> getOrderAdmins(UserDetails user) {
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();
        var orders = orderSingleRepo.findAllByPharmacy_Id(client.getPharmacyId());

        return orders.stream()
                .map(o -> {
                    var dto = new OrderGetAdminDto();
                    dto.setEmail(o.getOrder().getUser().getUsername());
                    dto.setFullName(o.getOrder().getUser().getFirstname()+ " " + o.getOrder().getUser().getLastname());
                    dto.setAddress(o.getOrder().getAddress().getStreet() + " " + o.getOrder().getAddress().getHouseNumber());
                    dto.setApplicationNumber("№" + o.getOrderSingleId());

                    return dto;
                }).toList();
    }

    private ProductGetDto mapProduct(ProductEntity entity) {
        ProductGetDto dto = new ProductGetDto();
        dto.setProductId(entity.getId());
        dto.setTitle(entity.getName() + " " + entity.getDosage() + " " + entity.getForm().getString());
        dto.setPrice(getPriceFromList(entity.getPrices()));
        dto.setSubtitle(entity.getUsageInfo());

        if (CollectionUtils.isEmpty(entity.getPhotoPaths())) {
            dto.setPhotoUrl("https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg");
        } else {
            dto.setPhotoUrl(entity.getPhotoPaths().get(0));
        }
        dto.setShortInfo(entity.getShortInfo());

        return dto;
    }

    private String getPriceFromList(List<ProductPriceEntity> prices) {
        return prices.stream()
                .mapToDouble(p-> p.getPrice().doubleValue())
                .min().orElse(0) + "₸";
    }

    private List<ReviewGetDto> getReviews(List<ReviewEntity> reviews) {
        return  reviews.stream()
                .map(r -> {
                    ReviewGetDto dto = new ReviewGetDto();
                    dto.setUsername(r.getUser().getFirstname() + " " + r.getUser().getLastname());
                    dto.setComment(r.getComment());
                    dto.setStars(r.getStars());

                    return dto;
                })
                .toList();
    }

    private List<PharmacyGetDto> getPharmacies(List<ProductPriceEntity> prices) {

        return prices.stream()
                .map(p -> {
                    PharmacyEntity entity = p.getPharmacy();
                    List<ReviewEntity> reviews = reviewRepo.findByPharmacy(entity);
                    PharmacyGetDto dto = new PharmacyGetDto();
                    dto.setPharmacyId(entity.getId());
                    dto.setReviews(reviews.size());
                    dto.setStars(getPharmaciesStars(reviews));
                    dto.setPickupDate(LocalDate.now().format(DateTimeFormatter.ofPattern("dd MMMM")));
                    dto.setDeliveryDate(LocalDate.now().plusDays(2).format(DateTimeFormatter.ofPattern("dd MMMM")));
                    dto.setName(entity.getName());
                    dto.setPrice(p.getPrice() + "₸");
                    dto.setAddress(entity.getAddress());

                    return dto;
                })
                .toList();
    }

    private int getPharmaciesStars(List<ReviewEntity> reviews) {
        return (int) reviews.stream()
                .mapToInt(ReviewEntity::getStars)
                .average()
                .orElse(0);
    }
}
