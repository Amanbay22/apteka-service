package kz.yasmin.apteka.service.impl;

import jakarta.persistence.EntityNotFoundException;

import java.util.Base64;
import java.util.Set;

import kz.yasmin.apteka.config.security.JwtService;
import kz.yasmin.apteka.domain.entity.Roles;
import kz.yasmin.apteka.domain.entity.UserEntity;
import kz.yasmin.apteka.domain.repo.UserRepository;
import kz.yasmin.apteka.dto.auth.AuthRequestDto;
import kz.yasmin.apteka.dto.auth.AuthResponseDto;
import kz.yasmin.apteka.dto.auth.RegRequestDto;
import kz.yasmin.apteka.dto.auth.ResetPasswordDto;
import kz.yasmin.apteka.exception.UnauthorizedException;
import kz.yasmin.apteka.service.IAuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements IAuthService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final JwtService jwtService;
  private final AuthenticationManager authenticationManager;

  @Transactional
  public AuthResponseDto register(RegRequestDto request, Set<Roles> roles) {

    var userOpt = userRepository.findByUsername(request.getPhoneNumber());

    if (userOpt.isEmpty()) {
      var user = new UserEntity();

      user.setFirstname(request.getFirstname());
      user.setLastname(request.getLastname());
      user.setSex(request.getSex());
      user.setUsername(request.getPhoneNumber());
      user.setRoles(roles);
      user.setPassword(passwordEncoder.encode(request.getPassword()));
      user.setPharmacyId(request.getPharmacyId());

      userRepository.save(user);

      return new AuthResponseDto(jwtService.generateToken(user), user.getRoles().stream().findFirst().get().toString());
    }

    return new AuthResponseDto("already have account", null);
  }

  @Transactional(readOnly = true)
  public AuthResponseDto authenticate(AuthRequestDto request) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            request.getUsername(),
            request.getPassword()
        )
    );

    UserEntity user = userRepository.findByUsername(request.getUsername())
        .orElseThrow(EntityNotFoundException::new);

    return new AuthResponseDto(jwtService.generateToken(user), user.getRoles().stream().findFirst().get().toString());
  }

  @Override
  @Transactional
  public void resetPassword(ResetPasswordDto dto) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            dto.getUsername(),
            dto.getPassword()
        )
    );

    UserEntity user = userRepository.findByUsername(dto.getUsername())
        .orElseThrow(EntityNotFoundException::new);

    user.setPassword(passwordEncoder.encode(dto.getNewPassword()));
  }

  @Override
  @Transactional
  public AuthResponseDto registerAdmin(RegRequestDto regDto, String basic) {

    String pair = new String(Base64.getDecoder().decode(basic.substring(6)));

    String username = pair.split(":")[0];;
    String password = pair.split(":")[1];;

    if (username.equals("admin") && password.equals("12345")) {

      return register(regDto, Set.of(Roles.ROLE_ADMIN));
    } else {
      throw new UnauthorizedException();
    }

  }
}
