package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.domain.entity.AddressEntity;
import kz.yasmin.apteka.domain.entity.PharmacyEntity;
import kz.yasmin.apteka.domain.entity.ReviewEntity;
import kz.yasmin.apteka.domain.repo.AddressRepo;
import kz.yasmin.apteka.domain.repo.PharmacyRepo;
import kz.yasmin.apteka.domain.repo.ReviewRepo;
import kz.yasmin.apteka.dto.pharmacy.PharmacyCreateDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyGetDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyRs;
import kz.yasmin.apteka.dto.pharmacy.PharmacySimpleDto;
import kz.yasmin.apteka.service.IPharmacyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PharmacyServiceImpl implements IPharmacyService {

    private final PharmacyRepo pharmacyRepo;
    private final AddressRepo addressRepo;
    private final ReviewRepo reviewRepo;

    @Override
    public PharmacyRs createPharmacy(PharmacyCreateDto dto) {
        var pharmacy = new PharmacyEntity();
        pharmacy.setName(dto.getName());
        pharmacy.setAddress(dto.getAddress());
        pharmacy.setRating(0);
        PharmacyEntity save = pharmacyRepo.save(pharmacy);
        var address = new AddressEntity();
        address.setStreet(dto.getAddress());
        addressRepo.save(address);

        return new PharmacyRs(200, "Pharmacy created", save.getId());
    }

    @Override
    public List<PharmacySimpleDto> getPharmacies() {
        List<PharmacyEntity> all = pharmacyRepo.findAll();
        return all.stream().map(entity -> {
            PharmacySimpleDto dto = new PharmacySimpleDto();
            dto.setPharmacyId(entity.getId());
            dto.setName(entity.getName());

            return dto;
        }).toList();
    }

}