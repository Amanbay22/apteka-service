package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.dto.survey.SurveyDto;
import kz.yasmin.apteka.service.ISurveyService;
import kz.yasmin.apteka.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SurveyServiceImpl implements ISurveyService {

    private final IUserService userService;

    @Override
    public void saveSurveyData(SurveyDto dto) {
        userService.setUserIssuesByUsername(dto.getIssueTypes());
    }
}
