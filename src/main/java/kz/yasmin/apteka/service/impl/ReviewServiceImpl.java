package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.domain.entity.ReviewEntity;
import kz.yasmin.apteka.domain.entity.UserEntity;
import kz.yasmin.apteka.domain.repo.PharmacyRepo;
import kz.yasmin.apteka.domain.repo.ProductRepo;
import kz.yasmin.apteka.domain.repo.ReviewRepo;
import kz.yasmin.apteka.domain.repo.UserRepository;
import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.review.ReviewCreateDto;
import kz.yasmin.apteka.service.IReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ReviewServiceImpl implements IReviewService {

    private final ReviewRepo repo;
    private final UserRepository userRepo;
    private final ProductRepo productRepo;
    private final PharmacyRepo pharmacyRepo;

    @Override
    public BasicRs createReview(UserDetails user, ReviewCreateDto dto) {
        ReviewEntity review = new ReviewEntity();
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();
        var product = productRepo.findById(dto.getProductId()).orElseThrow();
        var pharmacy = pharmacyRepo.findById(dto.getPharmacyId()).orElseThrow();

        review.setUser(client);
        review.setStars(dto.getStars());
        review.setProduct(product);
        review.setPharmacy(pharmacy);
        review.setComment(dto.getComment());
        repo.save(review);

        return new BasicRs(200, "Review success created");
    }
}
