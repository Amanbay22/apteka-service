package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.domain.entity.OrderStatusEnum;
import kz.yasmin.apteka.domain.repo.OrderSingleRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@EnableScheduling
@Service
@RequiredArgsConstructor
public class DeliveryService {

    private final OrderSingleRepo repo;

    @Scheduled(fixedDelay = 30, timeUnit = TimeUnit.MINUTES)
    public void changeStatusOnDelivery() {
        var all = repo.findAllByStatus(OrderStatusEnum.PAID);

        all.forEach(o -> {
            o.setStatus(OrderStatusEnum.DELIVERED);
            repo.save(o);
        });
    }
}
