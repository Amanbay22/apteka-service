package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.dto.file.FileGetDto;
import kz.yasmin.apteka.dto.file.FilesGetDto;
import kz.yasmin.apteka.service.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class UploadServiceImpl implements IUploadService {

    private final Path fileStorageLocation;
    private final Random random;

    @Autowired
    public UploadServiceImpl(Environment env) {
        this.random = new Random();
        this.fileStorageLocation = Paths.get(env.getProperty("./files", "./files"))
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    @Override
    public FileGetDto saveFile(MultipartFile file) {
        String fileName =
                new Date().getTime() + "-" + random.nextInt(10000) + "-file." + getFileExtension(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new RuntimeException(
                        "Sorry! Filename contains invalid path sequence " + fileName);
            }

            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            var dto = new FileGetDto();
            dto.setFileName("/files/" + fileName);

            return dto;
        } catch (IOException ex) {
            throw new RuntimeException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public FilesGetDto saveFiles(List<MultipartFile> files) {

        List<String> fileNames = new ArrayList<>();
        files.forEach(file -> {
            FileGetDto fileGetDto = saveFile(file);
            fileNames.add(fileGetDto.getFileName());
        });

        FilesGetDto dto = new FilesGetDto();
        dto.setFilesName(fileNames);

        return dto;
    }

    private String getFileExtension(String fileName) {
        if (fileName == null) {
            return null;
        }
        String[] fileNameParts = fileName.split("\\.");

        return fileNameParts[fileNameParts.length - 1];
    }
}
