package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.domain.entity.*;
import kz.yasmin.apteka.domain.repo.*;
import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.order.OrderGetDto;
import kz.yasmin.apteka.dto.order.OrderRs;
import kz.yasmin.apteka.dto.order.OrdersCreateDto;
import kz.yasmin.apteka.service.IOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements IOrderService {

    private final OrderRepo orderRepo;
    private final OrderSingleRepo orderSingleRepo;
    private final UserRepository userRepo;
    private final AddressRepo addressRepo;
    private final PharmacyRepo pharmacyRepo;
    private final ProductRepo productRepo;

    @Override
    public OrderRs createOrder(UserDetails user, OrdersCreateDto orders) {
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();
        AddressEntity address;

        if (ObjectUtils.isEmpty(orders.getAddressId())) {
            address = null;
        } else {
            address = addressRepo.findById(orders.getAddressId()).orElseThrow();
        }

        var order = new OrderEntity();

        order.setUser(client);
        order.setAddress(address);
        OrderEntity orderEntity = orderRepo.save(order);
        List<BigDecimal> prices = new ArrayList<>();
        List<OrderSingleEntity> orderSingleEntities = new ArrayList<>();
        orders.getOrders()
                .forEach(o -> {
                    var orderSingle = new OrderSingleEntity();
                    var pharmacy = pharmacyRepo.findById(o.getPharmacyId()).orElseThrow();
                    var product = productRepo.findById(o.getProductId()).orElseThrow();
                    BigDecimal price = product.getPrices().stream().filter(p -> p.getPharmacy().getId().equals(pharmacy.getId())).findFirst().orElseThrow().getPrice();
                    prices.add(price);
                    orderSingle.setPrice(price);
                    orderSingle.setOrder(orderEntity);
                    orderSingle.setPharmacy(pharmacy);
                    orderSingle.setProduct(product);
                    orderSingle.setStatus(OrderStatusEnum.NEW);
                    orderSingle.setCreatedDate(LocalDateTime.now());

                    orderSingleEntities.add(orderSingle);
                });

        orderSingleRepo.saveAll(orderSingleEntities);

        double totalPrice = prices.stream()
                .mapToDouble(BigDecimal::doubleValue)
                .sum();

        orderEntity.setTotalPrice(BigDecimal.valueOf(totalPrice));
        orderRepo.save(orderEntity);

        return new OrderRs(200, "Order successfully created", order.getId());
    }

    @Override
    @Transactional
    public BasicRs payOrder(Long orderId) {
        var orders = orderSingleRepo.findAllByOrder_Id(orderId);
        orders.forEach(o -> o.setStatus(OrderStatusEnum.PAID));

        return new BasicRs(200, "Order paid");
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderGetDto> getOrders(UserDetails user) {
        var client = userRepo.findByUsername(user.getUsername()).orElseThrow();
        List<OrderEntity> orders = orderRepo.findAllByUser(client);
        return orders.stream()
                .flatMap(o -> orderSingleRepo.findAllByOrder_Id(o.getId()).stream()
                        .map(this::mapOrderGet)
                )
                .toList();
    }

    private OrderGetDto mapOrderGet(OrderSingleEntity entity) {
        OrderGetDto dto = new OrderGetDto();
        OrderEntity order = entity.getOrder();

        dto.setDate(entity.getCreatedDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy hh:mm")));
        dto.setOrderSingleId(entity.getOrderSingleId());
        dto.setPrice(entity.getPrice() + "₸");
        dto.setProductId(entity.getProduct().getId());
        dto.setPharmacyId(entity.getPharmacy().getId());

        ProductEntity product = entity.getProduct();

        if (CollectionUtils.isEmpty(product.getPhotoPaths())) {
            dto.setPhotoUrl("https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg");
        } else {
            dto.setPhotoUrl(product.getPhotoPaths().get(0));
        }

        dto.setProductName(product.getName() + " " + product.getDosage() + " " + product.getForm().getString());
        dto.setStatus(entity.getStatus());
        if (!ObjectUtils.isEmpty(dto.getAddress())) {
            dto.setAddress(order.getAddress().getStreet() + " " + order.getAddress().getHouseNumber());
        } else {
            dto.setAddress(entity.getPharmacy().getAddress());
        }
        dto.setOrderId(order.getId());

        return dto;
    }
}
