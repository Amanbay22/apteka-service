package kz.yasmin.apteka.service.impl;

import kz.yasmin.apteka.domain.entity.OrderSingleEntity;
import kz.yasmin.apteka.domain.entity.OrderStatusEnum;
import kz.yasmin.apteka.domain.repo.OrderRepo;
import kz.yasmin.apteka.domain.repo.OrderSingleRepo;
import kz.yasmin.apteka.dto.admin.AdminDashboardData;
import kz.yasmin.apteka.dto.order.OrderGetAdminDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdminService {
    private final OrderSingleRepo orderSingleRepo;

    public AdminDashboardData getDashboard() {
        var orders = orderSingleRepo.findAll();

        Map<OrderStatusEnum, List<OrderSingleEntity>> map = orders.stream()
                .collect(Collectors.groupingBy(OrderSingleEntity::getStatus));
        List<OrderSingleEntity> empty = new ArrayList<>();
        var dashboard = new AdminDashboardData();
        Integer active = map.getOrDefault(OrderStatusEnum.NEW, empty).size()
                + map.getOrDefault(OrderStatusEnum.PAID, empty).size()
                + map.getOrDefault(OrderStatusEnum.DELIVERING, empty).size();
        dashboard.setActive(active);
        dashboard.setCompleted(map.getOrDefault(OrderStatusEnum.DELIVERED, empty).size());
        dashboard.setCanceled(map.getOrDefault(OrderStatusEnum.FAILED, empty).size());

        BigDecimal totalPrice = BigDecimal.valueOf(orders.stream()
                .mapToDouble(o-> o.getPrice().doubleValue())
                .sum());
        dashboard.setRevenue(totalPrice);

        return dashboard;
    }

    public  List<OrderGetAdminDto> getOrders() {
        var orders = orderSingleRepo.findAll();

        return orders.stream()
                .map(o -> {
                    var dto = new OrderGetAdminDto();
                    dto.setEmail(o.getOrder().getUser().getUsername());
                    dto.setFullName(o.getOrder().getUser().getFirstname()+ " " + o.getOrder().getUser().getLastname());
                    dto.setAddress(o.getOrder().getAddress().getStreet() + " " + o.getOrder().getAddress().getHouseNumber());
                    dto.setApplicationNumber("№" + o.getOrderSingleId());

                    return dto;
                }).toList();
    }
}
