package kz.yasmin.apteka.service.impl;

import jakarta.persistence.EntityNotFoundException;
import kz.yasmin.apteka.domain.entity.ProductTypeEnum;
import kz.yasmin.apteka.domain.entity.UserEntity;
import kz.yasmin.apteka.domain.repo.UserRepository;
import kz.yasmin.apteka.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public void setUserIssuesByUsername(List<ProductTypeEnum> issues) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        UserEntity user = userRepository.findByUsername(username).orElseThrow(EntityNotFoundException::new);
        user.setIssueTypes(issues);
    }
}
