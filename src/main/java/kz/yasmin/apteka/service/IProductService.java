package kz.yasmin.apteka.service;

import kz.yasmin.apteka.domain.entity.ProductTypeEnum;
import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.order.OrderGetAdminDto;
import kz.yasmin.apteka.dto.order.OrderGetDto;
import kz.yasmin.apteka.dto.product.*;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface IProductService {
    BasicRs createProduct(UserDetails user, ProductCreateDto dto);
    List<ProductGetDto> getProductByType(ProductTypeEnum type);
    ProductFullDto getProductById(Long productId);
    List<ProductGetDto> getProductByClientIssues(UserDetails user);
    List<ProductSearchDto> getProductSearch(String search);
    List<ProductByPharmacyDto> getProductByPharmacy(UserDetails user);
    List<OrderGetAdminDto> getOrderAdmins(UserDetails user);
}
