package kz.yasmin.apteka.service;

import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.order.OrderGetDto;
import kz.yasmin.apteka.dto.order.OrderRs;
import kz.yasmin.apteka.dto.order.OrdersCreateDto;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface IOrderService {
    OrderRs createOrder(UserDetails user, OrdersCreateDto orders);
    BasicRs payOrder(Long orderId);
    List<OrderGetDto> getOrders(UserDetails user);
}
