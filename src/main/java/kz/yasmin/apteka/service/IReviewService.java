package kz.yasmin.apteka.service;

import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.review.ReviewCreateDto;
import org.springframework.security.core.userdetails.UserDetails;

public interface IReviewService {
    BasicRs createReview(UserDetails user, ReviewCreateDto dto);
}
