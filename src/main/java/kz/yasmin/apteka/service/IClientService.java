package kz.yasmin.apteka.service;

import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.client.AddressCreateDto;
import kz.yasmin.apteka.dto.client.AddressRs;
import kz.yasmin.apteka.dto.client.ClientGetDto;
import kz.yasmin.apteka.dto.client.ClientInfoDto;
import org.springframework.security.core.userdetails.UserDetails;

public interface IClientService {
    AddressRs createClientAddress(UserDetails user, AddressCreateDto dto);
    BasicRs changeClientInfo(UserDetails user, ClientInfoDto dto);
    ClientGetDto getClientInfo(UserDetails user);
}
