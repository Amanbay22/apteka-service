package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.domain.entity.ProductTypeEnum;
import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.product.ProductCreateDto;
import kz.yasmin.apteka.dto.product.ProductFullDto;
import kz.yasmin.apteka.dto.product.ProductGetDto;
import kz.yasmin.apteka.dto.product.ProductSearchDto;
import kz.yasmin.apteka.service.IProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
@CrossOrigin
public class ProductController {

    private final IProductService productService;

    @PreAuthorize("hasRole('ROLE_PHARMACY_ADMIN')")
    @PostMapping
    public ResponseEntity<BasicRs> createProduct(@AuthenticationPrincipal UserDetails userDetails,
                                                  @RequestBody ProductCreateDto dto) {

        return ResponseEntity.ok(productService.createProduct(userDetails, dto));
    }

    @GetMapping
    public ResponseEntity<List<ProductGetDto>> getProductsByType(@RequestParam ProductTypeEnum type) {

        return ResponseEntity.ok(productService.getProductByType(type));
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductFullDto> getProductById(@PathVariable Long productId) {
        return ResponseEntity.ok(productService.getProductById(productId));
    }

    @GetMapping("/recommend")
    public ResponseEntity<List<ProductGetDto>> getReccomend(@AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.ok(productService.getProductByClientIssues(userDetails));
    }

    @GetMapping("/search")
    public ResponseEntity<List<ProductSearchDto>> getSearchProduct(@RequestParam String search) {
        return ResponseEntity.ok(productService.getProductSearch(search));
    }
 }
