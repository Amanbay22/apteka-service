package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.dto.order.OrderGetAdminDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyCreateDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyGetDto;
import kz.yasmin.apteka.dto.pharmacy.PharmacyRs;
import kz.yasmin.apteka.dto.pharmacy.PharmacySimpleDto;
import kz.yasmin.apteka.dto.product.ProductByPharmacyDto;
import kz.yasmin.apteka.dto.product.ProductFullDto;
import kz.yasmin.apteka.service.IPharmacyService;
import kz.yasmin.apteka.service.IProductService;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pharmacy")
@CrossOrigin
public class PharmacyController {

    private final IPharmacyService pharmacyService;
    private final IProductService productService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    public ResponseEntity<PharmacyRs> createPharmacy(@RequestBody PharmacyCreateDto dto) {
        return ResponseEntity.ok(pharmacyService.createPharmacy(dto));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public ResponseEntity<List<PharmacySimpleDto>> getPharmacy() {
        return ResponseEntity.ok(pharmacyService.getPharmacies());
    }

    @PreAuthorize("hasRole('ROLE_PHARMACY_ADMIN')")
    @GetMapping("/products")
    public ResponseEntity<List<ProductByPharmacyDto>> getProducts(@AuthenticationPrincipal UserDetails user) {
        return ResponseEntity.ok(productService.getProductByPharmacy(user));
    }

    @PreAuthorize("hasRole('ROLE_PHARMACY_ADMIN')")
    @GetMapping("/orders")
    public ResponseEntity<List<OrderGetAdminDto>> getOrders(@AuthenticationPrincipal UserDetails user) {
        return ResponseEntity.ok(productService.getOrderAdmins(user));
    }

}
