package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.dto.survey.SurveyDto;
import kz.yasmin.apteka.service.ISurveyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/survey")
@RequiredArgsConstructor
@CrossOrigin
public class SurveyController {

    private final ISurveyService surveyService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping
    public ResponseEntity<Void> saveSurveyData(@RequestBody SurveyDto dto) {
        surveyService.saveSurveyData(dto);

        return ResponseEntity.ok().build();
    }
}
