package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.dto.admin.AdminDashboardData;
import kz.yasmin.apteka.dto.order.OrderGetAdminDto;
import kz.yasmin.apteka.service.impl.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {

    private final AdminService adminService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/dashboard")
    public ResponseEntity<AdminDashboardData> getDashboard() {
        return ResponseEntity.ok(adminService.getDashboard());
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/orders")
    public ResponseEntity<List<OrderGetAdminDto>> getOrders() {
        return ResponseEntity.ok(adminService.getOrders());
    }
}
