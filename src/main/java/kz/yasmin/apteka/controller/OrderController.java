package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.order.OrderGetDto;
import kz.yasmin.apteka.dto.order.OrderRs;
import kz.yasmin.apteka.dto.order.OrdersCreateDto;
import kz.yasmin.apteka.service.IOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
@CrossOrigin
public class OrderController {

    private final IOrderService orderService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping
    public ResponseEntity<OrderRs> createOrder(@AuthenticationPrincipal UserDetails userDetails, @RequestBody OrdersCreateDto orders) {
        return ResponseEntity.ok(orderService.createOrder(userDetails, orders));
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{orderId}")
    public ResponseEntity<BasicRs> payOrder(@PathVariable Long orderId) {
        return ResponseEntity.ok(orderService.payOrder(orderId));
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping
    public ResponseEntity<List<OrderGetDto>> getOrders(@AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.ok(orderService.getOrders(userDetails));
    }
}
