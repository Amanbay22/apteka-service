package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.client.AddressCreateDto;
import kz.yasmin.apteka.dto.client.AddressRs;
import kz.yasmin.apteka.dto.client.ClientGetDto;
import kz.yasmin.apteka.dto.client.ClientInfoDto;
import kz.yasmin.apteka.service.IClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/client")
@RequiredArgsConstructor
@CrossOrigin
public class ClientController {

    private final IClientService clientService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/address")
    public ResponseEntity<AddressRs> addClientAddress(@RequestBody AddressCreateDto dto, @AuthenticationPrincipal UserDetails user) {
        return ResponseEntity.ok(clientService.createClientAddress(user, dto));
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PatchMapping
    public ResponseEntity<BasicRs> changeClientInfo(@RequestBody ClientInfoDto dto, @AuthenticationPrincipal UserDetails user) {
        return ResponseEntity.ok(clientService.changeClientInfo(user, dto));
    }

    @PreAuthorize("hasAnyRole({'ROLE_USER', 'ROLE_ADMIN', 'ROLE_PHARMACY_ADMIN'})")
    @GetMapping
    public ResponseEntity<ClientGetDto> getClientInfo(@AuthenticationPrincipal UserDetails user) {
        return ResponseEntity.ok(clientService.getClientInfo(user));
    }
}
