package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.dto.file.FileGetDto;
import kz.yasmin.apteka.dto.file.FilesGetDto;
import kz.yasmin.apteka.service.IUploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
@CrossOrigin
public class FileController {
    private final IUploadService uploadService;

    @PostMapping("/upload")
    public ResponseEntity<FileGetDto> uploadFile(MultipartFile file) {
        return ResponseEntity.ok(uploadService.saveFile(file));
    }

    @PostMapping("/upload-multiple")
    public ResponseEntity<FilesGetDto> uploadFiles(List<MultipartFile> files) {
        return ResponseEntity.ok(uploadService.saveFiles(files));
    }
}
