package kz.yasmin.apteka.controller;

import io.swagger.v3.oas.annotations.Operation;
import kz.yasmin.apteka.domain.entity.Roles;
import kz.yasmin.apteka.dto.auth.AuthRequestDto;
import kz.yasmin.apteka.dto.auth.AuthResponseDto;
import kz.yasmin.apteka.dto.auth.RegRequestDto;
import kz.yasmin.apteka.dto.auth.ResetPasswordDto;
import kz.yasmin.apteka.service.IAuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@CrossOrigin
public class AuthController {

    private final IAuthService authService;

    @PostMapping
    public ResponseEntity<AuthResponseDto> authenticate(@RequestBody AuthRequestDto request) {
        return ResponseEntity.ok(authService.authenticate(request));
    }

    @PostMapping("/register")
    public ResponseEntity<AuthResponseDto> register(@RequestBody RegRequestDto request) {
        return ResponseEntity.ok(authService.register(request, Set.of(Roles.ROLE_USER)));
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/resetPassword")
    public ResponseEntity<Void> resetPassword(@RequestBody ResetPasswordDto resetDto) {
        authService.resetPassword(resetDto);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Admin registration")
    @PostMapping("/reg-admin")
    public ResponseEntity<AuthResponseDto> regAdmin(@RequestBody RegRequestDto regDto,
                                                       @RequestHeader(value = HttpHeaders.AUTHORIZATION) String basic) {
        return ResponseEntity.ok(authService.registerAdmin(regDto, basic));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Operation(summary = "Pharmacy admin registration")
    @PostMapping("/reg-pharmacy-admin")
    public ResponseEntity<AuthResponseDto> regAdminPharmacy(@RequestBody RegRequestDto request) {
        return ResponseEntity.ok(authService.register(request, Set.of(Roles.ROLE_PHARMACY_ADMIN)));
    }

}
