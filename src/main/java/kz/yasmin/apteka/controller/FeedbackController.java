package kz.yasmin.apteka.controller;

import kz.yasmin.apteka.dto.BasicRs;
import kz.yasmin.apteka.dto.review.ReviewCreateDto;
import kz.yasmin.apteka.service.IReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/feedback")
@RequiredArgsConstructor
@CrossOrigin
public class FeedbackController {

    private final IReviewService reviewService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping
    public ResponseEntity<BasicRs> createReview(@AuthenticationPrincipal UserDetails user,
                                                @RequestBody ReviewCreateDto dto) {

        return ResponseEntity.ok(reviewService.createReview(user,dto));
    }
}
