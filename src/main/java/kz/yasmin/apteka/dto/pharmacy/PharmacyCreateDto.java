package kz.yasmin.apteka.dto.pharmacy;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PharmacyCreateDto {
    private String name;
    private String address;
}
