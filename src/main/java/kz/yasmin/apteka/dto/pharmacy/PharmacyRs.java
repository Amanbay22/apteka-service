package kz.yasmin.apteka.dto.pharmacy;

import kz.yasmin.apteka.dto.BasicRs;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class PharmacyRs extends BasicRs {
    private Long pharmacyId;

    public PharmacyRs(int status, String message, Long pharmacyId) {
        super(status, message);
        this.pharmacyId = pharmacyId;
    }
}
