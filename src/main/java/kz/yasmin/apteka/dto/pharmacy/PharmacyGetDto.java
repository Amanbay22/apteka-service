package kz.yasmin.apteka.dto.pharmacy;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PharmacyGetDto {
    private Long pharmacyId;
    private String name;
    private double stars;
    private int reviews;
    private String price;
    private String deliveryDate;
    private String pickupDate;
    private String address;
}
