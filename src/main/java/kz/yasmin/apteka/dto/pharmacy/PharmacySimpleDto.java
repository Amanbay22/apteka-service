package kz.yasmin.apteka.dto.pharmacy;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PharmacySimpleDto {
    private Long pharmacyId;
    private String name;
}
