package kz.yasmin.apteka.dto.survey;

import kz.yasmin.apteka.domain.entity.ProductTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SurveyDto {
    private List<ProductTypeEnum> issueTypes;
}
