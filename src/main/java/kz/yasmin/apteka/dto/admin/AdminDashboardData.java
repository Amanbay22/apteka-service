package kz.yasmin.apteka.dto.admin;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class AdminDashboardData {
    private Integer active;
    private Integer completed;
    private Integer canceled;
    private BigDecimal revenue;
}
