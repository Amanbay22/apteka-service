package kz.yasmin.apteka.dto.order;

import kz.yasmin.apteka.dto.BasicRs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderRs extends BasicRs {
    private Long orderId;

    public OrderRs(int status, String message, Long orderId) {
        super(status, message);
        this.orderId = orderId;
    }
}
