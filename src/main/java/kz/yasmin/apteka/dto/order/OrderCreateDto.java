package kz.yasmin.apteka.dto.order;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderCreateDto {
    private Long productId;
    private Long pharmacyId;
}
