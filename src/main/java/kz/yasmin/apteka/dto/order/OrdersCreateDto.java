package kz.yasmin.apteka.dto.order;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class OrdersCreateDto {
    private List<OrderCreateDto> orders;
    private Long addressId;
}
