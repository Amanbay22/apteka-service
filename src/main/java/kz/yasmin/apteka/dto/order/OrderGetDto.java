package kz.yasmin.apteka.dto.order;

import kz.yasmin.apteka.domain.entity.OrderStatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class OrderGetDto {
    private Long orderId;
    private Long orderSingleId;
    private Long productId;
    private Long pharmacyId;
    private String productName;
    private String price;
    private String date;
    private String address;
    private String photoUrl;
    private OrderStatusEnum status;
}
