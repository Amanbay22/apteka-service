package kz.yasmin.apteka.dto.order;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderGetAdminDto {
    private String fullName;
    private String email;
    private String address;
    private String applicationNumber;
}
