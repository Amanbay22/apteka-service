package kz.yasmin.apteka.dto.file;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FileGetDto {
    private String fileName;
}
