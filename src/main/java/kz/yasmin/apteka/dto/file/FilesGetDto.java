package kz.yasmin.apteka.dto.file;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class FilesGetDto {
    private List<String> filesName;
}
