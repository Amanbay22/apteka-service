package kz.yasmin.apteka.dto.client;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddressCreateDto {
    private String district;
    private String street;
    private String houseNumber;
    private String apartmentNumber;
    private String comment;
}
