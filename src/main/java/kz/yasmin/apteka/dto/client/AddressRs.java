package kz.yasmin.apteka.dto.client;

import kz.yasmin.apteka.dto.BasicRs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AddressRs extends BasicRs {
    private Long addressId;

    public AddressRs(int status, String message, Long addressId) {
        super(status, message);
        this.addressId = addressId;
    }
}
