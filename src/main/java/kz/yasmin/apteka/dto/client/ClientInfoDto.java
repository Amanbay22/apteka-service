package kz.yasmin.apteka.dto.client;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClientInfoDto {
    private String firstname;
    private String lastname;
    private String sex;
    private String phoneNumber;
}
