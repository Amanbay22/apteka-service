package kz.yasmin.apteka.dto.client;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ClientGetDto {
    private String firstname;
    private String lastname;
    private String sex;
    private String phoneNumber;
    private List<AddressDto> addresses;
}
