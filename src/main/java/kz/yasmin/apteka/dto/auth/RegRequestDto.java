package kz.yasmin.apteka.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegRequestDto {
  private String firstname;
  private String lastname;
  private String phoneNumber;
  private String sex;
  private String password;
  private Long pharmacyId;
}
