package kz.yasmin.apteka.dto.auth;

import kz.yasmin.apteka.domain.entity.Roles;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthResponseDto {
  private String token;
  private String role;
}
