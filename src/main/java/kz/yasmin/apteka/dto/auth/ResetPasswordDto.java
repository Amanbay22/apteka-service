package kz.yasmin.apteka.dto.auth;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResetPasswordDto {
  private String username;
  private String password;
  private String newPassword;
}
