package kz.yasmin.apteka.dto.review;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewCreateDto {
    private Long productId;
    private Long pharmacyId;
    private int stars;
    private String comment;
}
