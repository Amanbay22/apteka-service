package kz.yasmin.apteka.dto.review;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReviewGetDto {
    private String username;
    private int stars;
    private String comment;
}
