package kz.yasmin.apteka.dto.product;

import kz.yasmin.apteka.domain.entity.ProductFormEnum;
import kz.yasmin.apteka.domain.entity.ProductTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class ProductCreateDto {
    private String name;
    private BigDecimal price;
    private int dosage;
    private String dosagePeriod;
    private int dosageChild;
    private int dosageAdult;
    private String manufacturer;
    private String description;
    private String usageInfo;
    private String shortInfo;
    private List<String> photoPaths;
    private ProductFormEnum form;
    private ProductTypeEnum type;
}
