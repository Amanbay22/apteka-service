package kz.yasmin.apteka.dto.product;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductSearchDto {
    private String name;
    private Long productId;
}
