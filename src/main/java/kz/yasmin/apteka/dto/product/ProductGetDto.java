package kz.yasmin.apteka.dto.product;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductGetDto {
    private Long productId;
    private String photoUrl;
    private String subtitle;
    private String title;
    private String shortInfo;
    private String price;
}
