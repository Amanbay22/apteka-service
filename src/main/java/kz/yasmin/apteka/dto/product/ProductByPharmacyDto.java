package kz.yasmin.apteka.dto.product;

import kz.yasmin.apteka.domain.entity.ProductFormEnum;
import kz.yasmin.apteka.domain.entity.ProductTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductByPharmacyDto {
    private Long id;
    private int stars;
    private String name;
    private int dosage;
    private String dosagePeriod;
    private int dosageChild;
    private int dosageAdult;
    private String manufacturer;
    private String description;
    private String usageInfo;
    private String shortInfo;
    private ProductFormEnum form;
    private ProductTypeEnum type;
    private String price;
}
