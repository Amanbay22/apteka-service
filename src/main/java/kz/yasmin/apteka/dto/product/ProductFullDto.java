package kz.yasmin.apteka.dto.product;

import kz.yasmin.apteka.dto.pharmacy.PharmacyGetDto;
import kz.yasmin.apteka.dto.review.ReviewGetDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ProductFullDto {
    private String title;
    private String price;
    private String dosage;
    private String manufacturer;
    private String dosagePeriod;
    private String dosageFrom;
    private String dosageTo;
    private String description;
    private List<String> photoUrls;
    private List<PharmacyGetDto> pharmacies;
    private List<ReviewGetDto> reviews;
}
