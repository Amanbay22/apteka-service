package kz.yasmin.apteka.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDto {
    private String username;
    private String firstname;
    private String lastname;
}
