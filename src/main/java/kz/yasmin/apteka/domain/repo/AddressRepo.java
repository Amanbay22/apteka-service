package kz.yasmin.apteka.domain.repo;

import kz.yasmin.apteka.domain.entity.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepo extends JpaRepository<AddressEntity, Long> {
}
