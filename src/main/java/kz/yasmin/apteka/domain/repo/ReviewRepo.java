package kz.yasmin.apteka.domain.repo;

import kz.yasmin.apteka.domain.entity.PharmacyEntity;
import kz.yasmin.apteka.domain.entity.ProductEntity;
import kz.yasmin.apteka.domain.entity.ReviewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewRepo extends JpaRepository<ReviewEntity, Long> {
    List<ReviewEntity> findByProduct(ProductEntity product);
    List<ReviewEntity> findByPharmacy(PharmacyEntity pharmacy);
}
