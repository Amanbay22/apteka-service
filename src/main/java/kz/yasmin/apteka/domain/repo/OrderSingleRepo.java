package kz.yasmin.apteka.domain.repo;

import kz.yasmin.apteka.domain.entity.OrderEntity;
import kz.yasmin.apteka.domain.entity.OrderSingleEntity;
import kz.yasmin.apteka.domain.entity.OrderStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderSingleRepo extends JpaRepository<OrderSingleEntity, Long> {
    List<OrderSingleEntity> findAllByOrder_Id(Long orderId);
    List<OrderSingleEntity> findAllByStatus(OrderStatusEnum status);
    List<OrderSingleEntity> findAllByPharmacy_Id(Long pharmacyId);
}
