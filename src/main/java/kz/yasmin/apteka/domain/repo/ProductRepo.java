package kz.yasmin.apteka.domain.repo;

import kz.yasmin.apteka.domain.entity.ProductEntity;
import kz.yasmin.apteka.domain.entity.ProductPriceEntity;
import kz.yasmin.apteka.domain.entity.ProductTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ProductRepo extends JpaRepository<ProductEntity, Long> {
    List<ProductEntity> findAllByType(ProductTypeEnum type);
    Optional<ProductEntity> findByNameAndDosageAndType(String name, int dosage, ProductTypeEnum type);
    List<ProductEntity> findAllByTypeIn(List<ProductTypeEnum> type);
    List<ProductEntity> findTop10ByType(ProductTypeEnum type);
    List<ProductEntity> findAllByNameContainingIgnoreCase(String search);
}
