package kz.yasmin.apteka.domain.repo;

import kz.yasmin.apteka.domain.entity.PharmacyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PharmacyRepo extends JpaRepository<PharmacyEntity, Long> {
}
