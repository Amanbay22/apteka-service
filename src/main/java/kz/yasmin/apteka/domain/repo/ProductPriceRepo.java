package kz.yasmin.apteka.domain.repo;

import kz.yasmin.apteka.domain.entity.PharmacyEntity;
import kz.yasmin.apteka.domain.entity.ProductPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductPriceRepo extends JpaRepository<ProductPriceEntity, Long> {
    List<ProductPriceEntity> findAllByPharmacy_Id(Long pharmacyId);
}
