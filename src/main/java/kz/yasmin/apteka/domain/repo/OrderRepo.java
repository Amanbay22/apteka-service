package kz.yasmin.apteka.domain.repo;

import kz.yasmin.apteka.domain.entity.OrderEntity;
import kz.yasmin.apteka.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepo extends JpaRepository<OrderEntity, Long> {
    List<OrderEntity> findAllByUser(UserEntity user);
}
