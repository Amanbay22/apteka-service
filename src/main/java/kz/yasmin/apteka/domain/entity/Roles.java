package kz.yasmin.apteka.domain.entity;

public enum Roles {
  ROLE_USER, ROLE_ADMIN, ROLE_PHARMACY_ADMIN
}
