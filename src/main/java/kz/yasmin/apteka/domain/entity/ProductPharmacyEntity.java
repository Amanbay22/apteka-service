package kz.yasmin.apteka.domain.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "product_pharmacy")
@Data
@NoArgsConstructor
public class ProductPharmacyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private PharmacyEntity pharmacyEntity;
    @ManyToOne
    private ProductEntity productEntity;

    private BigDecimal price;
    private LocalDateTime deliveryDate;
    private LocalDateTime pickupDate;
}
