package kz.yasmin.apteka.domain.entity;

public enum OrderStatusEnum {
    NEW, PAID, DELIVERING, DELIVERED, FAILED
}
