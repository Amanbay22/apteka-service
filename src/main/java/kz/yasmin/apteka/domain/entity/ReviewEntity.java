package kz.yasmin.apteka.domain.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "reviews")
@Data
@NoArgsConstructor
public class ReviewEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String comment;
    private int stars;
    @ManyToOne
    private UserEntity user;
    @ManyToOne
    private ProductEntity product;
    @ManyToOne
    private PharmacyEntity pharmacy;
}
