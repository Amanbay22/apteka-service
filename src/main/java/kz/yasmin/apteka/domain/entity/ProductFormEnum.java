package kz.yasmin.apteka.domain.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductFormEnum {
    ML("мл"), MG("мг"), G("г");

    ProductFormEnum(String rus) {
        this.lang = rus;
    }

    private final String lang;

    @JsonValue
    public String getString() {
        return lang;
    }
}
