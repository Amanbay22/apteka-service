package kz.yasmin.apteka.domain.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Table(name = "product_prices")
@Data
@NoArgsConstructor
public class ProductPriceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long productPriceId;

    @ManyToOne
    private PharmacyEntity pharmacy;
    private BigDecimal price;

    @ManyToOne
    private ProductEntity product;
}
