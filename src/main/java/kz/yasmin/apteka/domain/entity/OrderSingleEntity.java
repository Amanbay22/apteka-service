package kz.yasmin.apteka.domain.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "order_single")
@Data
@NoArgsConstructor
public class OrderSingleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderSingleId;

    @ManyToOne
    private OrderEntity order;

    @ManyToOne
    private ProductEntity product;

    @ManyToOne
    private PharmacyEntity pharmacy;

    private BigDecimal price;

    @Enumerated(EnumType.STRING)
    private OrderStatusEnum status;

    private LocalDateTime createdDate;
}
