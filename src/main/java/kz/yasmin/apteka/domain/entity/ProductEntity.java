package kz.yasmin.apteka.domain.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "products")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int stars;
    private String name;
    private int dosage;
    private String dosagePeriod;
    private int dosageChild;
    private int dosageAdult;
    private String manufacturer;
    private String description;
    private String usageInfo;
    private String shortInfo;

    @ElementCollection
    private List<String> photoPaths;

    @Enumerated(EnumType.STRING)
    private ProductFormEnum form;

    @Enumerated(EnumType.STRING)
    private ProductTypeEnum type;

    @OneToMany
    private List<ProductPriceEntity> prices;
}
