package kz.yasmin.apteka.config;

import kz.yasmin.apteka.mapper.AddressMapper;
import kz.yasmin.apteka.mapper.ProductMapper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addConverter(new ProductMapper());
        modelMapper.addConverter(new AddressMapper());

        return modelMapper;
    }
}

