package kz.yasmin.apteka.config.security;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("jwt.token")
public record JwtProperties(String secret, int expiration) {
}
